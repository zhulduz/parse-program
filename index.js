var xmltv = require('xmltv')
var fs = require('fs')
var path = require('path')
var ramda = require('ramda')

import { contains } from 'ramda'

var input = fs.createReadStream(path.join(__dirname, '23-30march.xml'))
var parser = new xmltv.Parser()
input.pipe(parser)
const programmeArray = []

// Взять id каналов с сервиса https://epgguide.net/channels/
const channelTVIds = ['3sat.de', 'zdf.de', 'arte.de', 'ard.de']

// Подставить наши ids
const channelsAlphaOttIds = {
  '3sat.de': '5c7e3275495c6a0001dcedff', // done
  'zdf.de': '5c752c0f97373700010f26e1', // done
  'arte.de': '5c7e3331495c6a0001dcee37', // done
  'ard.de': '5c7e33c0495c6a0001dcee71', // done
  'kabel1.de': '5c7e3436495c6a0001dceebc', // done
  'kika.de': '5c7e34bc495c6a0001dcef06' // done
}

parser.on('programme', function(programme) {
  if (contains(programme.channel, channelIds)) {
    programmeArray.push({
      channel: channelsAlphaOttIds[programme.channel],
      title: programme.title[0],
      start: programme.start,
      stop: programme.end,
      description: programme.desc[0]
    })
  }
})

// все запишется в program.json
parser.on('end', function(programme) {
  fs.writeFile('program.json', JSON.stringify(programmeArray), err => {
    // throws an error, you could also catch it here
    if (err) throw err

    // success case, the file was saved
    console.log('Done!')
  })
})
